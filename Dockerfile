FROM  python:3.9

# create root directory for our project in the container
RUN mkdir /demo

# Set the working directory to /Rosenmeister
WORKDIR /demo_fastapi

# Copy the current directory contents into the container at /Rosenmeister
ADD . /demo_fastapi

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt


EXPOSE 5000

# run the command
CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "5000"]