# Async FastAPI service with SQLAlchemy

Example of an async service with FastAPI and SQLAlchemy. Use case of a Bookstore

- demo the router with dependencies injections ("Depends").
- demo  async features of SQLAlchemy

## Installing  and run the Project

From the main folder take the following steps:

       - 0. A working Python3 version with pip installed is assumed.

        - 1. Go the root folder with  a terminal.

        - 2. Install dependencies with the command : pip3 install -r requirements.txt

        - 3. Start the server with : python3 app.py






## run the Project

Simply run with python the app.py file.

Or build and run the container with with docker compose or podman-compose



TODO: adapt to postegres
